#
# A web app that allows the user to provide the profile token, see the version,connection status, and connection statistics. User can also trigger reconnection.
# Author: Rohit Kalbag
# Copyright (C) 2012-2020 OpenVPN Inc. All rights reserved.
#
from bash import bash
# A library that enables easy running and concatenation of bash commands in python https://pypi.org/project/bash/
import pam, re
# regex library
# python pam module https://pypi.org/project/python-pam/
from bottle import route, redirect, run, debug, auth_basic, template, request, error

def check(user, password):
    # function for HTTP basic auth check that will succeed if the credentials supplied correspond to a user on the system
    p = pam.pam()
    return p.authenticate(user,password)


@route('/')
#homepage route
@auth_basic(check)
# checks for basic auth success
def index():
    # get openvpn3 client version
    v = bash('openvpn3 version')
    ver = str(v.stdout.decode("utf-8")).strip()
# show error if version cannot be found
    if "(openvpn3)" not in ver:
        return template('err', error= 'OpenVPN Connector is not installed', errlist='')
# split version string to make list of lines
    ver_lines = ver.split("\n")
# check sessions list to see if the client is connected
    s = bash('openvpn3 sessions-list')
    s_decode = str(s.stdout.decode("utf-8"))
    if "No sessions available" in s_decode:
       # render homepage with just version and status of disconnected without stats because session is not connected
        return template('home', version=ver_lines, status='Disconnected', stats= '')
    else:
      # get the session path by splitting stdout based on newline
      split_session_info = s_decode.split("\n")
      # get the path directory from the second line of stdout
      session_path = split_session_info[1].split("Path: ")[1]
      # use session path to get statistics
      command = 'sudo openvpn3 session-stats --session-path ' + session_path
      d = bash(command)
      # create a list of stdout lines for the template to iterate over
      stat = str(d.stdout.decode("utf-8")).split("\n")
      #remove unneeded lines
      del stat[0:2]
      del stat[-3]
      # render the home page template with the version, connected session status, and statistics
      return template('home', version=ver_lines, status='Connected', stats= stat)

@route('/submit')
# route for submission of token
@auth_basic(check)
# checks for basic auth success
def submit():
    t = request.params.get('token')
    # check that input does not contain any spaces and has length of 84
    safe = re.search("^\S{84}$", t)
    if safe:
        command = 'sudo openvpn-connector-setup --token {}'.format(t)
        d = bash('{}'.format(command))
        # expected successfull stdout outcome: Downloading OpenVPN Cloud Connector profile ... Done Saving profile to "/etc/openvpn3/autoload/connector.conf" ... Done Saving openvpn3-autoload config to "/etc/openvpn3/autoload/connector.autoload" ... Done Enabling openvpn3-autoload.service during boot ... Done Starting openvpn3-autoload.service ... Done
        tok_out = str(v.stdout.decode("utf-8")).strip()
        if "Done Saving profile " in tok_out: #succesful
          return template('res', outcome="OpenVPN Cloud Connector profile has been downloaded and saved")
        else: #unsuccessful
          return template('err', error = '',errlist=tok_out.split("\n"))
    else: #token string is not valid
        return template('err', error="Invalid token format", errlist='')

@route('/restart')
@auth_basic(check)
# checks for basic auth success
def restart():
    s = bash('openvpn3 sessions-list')
    s_decode = str(s.stdout.decode("utf-8"))
    split_session_info = s_decode.split("\n")
    session_path = split_session_info[1].split("Path: ")[1]
    if  "Connection, Client connected" in s_decode:
        command = 'sudo openvpn3 session-manage --session-path ' + session_path + ' --restart'
        d = bash(command)
        # expected successfull stdout outcome: Restarting session: /net/openvpn/v3/sessions/a33d91dfs1276s499fsb20cs3c4000b2b6b9 Connected
        res_ret = str(d.stdout.decode("utf-8")).strip()
        if "Connected" in res_ret:
           return template('res', outcome= "Restart successful")
        else:
           return template('err', error = '',errlist=res_ret.split("\n"))

@error(403)
def mistake403(code):
    return 'There is a mistake in your url!'


@error(404)
def mistake404(code):
    return 'Sorry, this page does not exist!'

#debug(True)

# find public IP address to run server
#pubip = str(bash('curl https://openvpn.net/as_resources/ip.php').stdout.decode("utf-8")).split("\n")[0].strip()
# check that response contains IP address
#if re.search("^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", pubip):
   # run(host=pubip, port=80,reloader=True)
#    run(host=pubip, port=80)
run(host='0.0.0.0', port=80)
