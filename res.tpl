<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>OpenVPN Connector</title>
  </head>
  <table>
  <tbody>
  <tr>
  <td>
  <p><img style="display: block; margin-left: auto; margin-right: auto;" src="https://openvpn.net/images/openvpn.png" alt="OpenVPN Logo" width="292" height="56" /></p>
  <h1 style="text-align: center;">Connector for OpenVPN Cloud</h1>
  </td>
  </tr>
  <tr>
  <td>
  <h1>Outcome:</h1>
  {{outcome}}
  <form action="/" method="GET"><input name="submit" type="submit" value="Back" /></form></td>
</tr>
  <tr>
  <td>
  <div>
<p><span style="color: #ff0000;">For improved security, please set network firewall to disable HTTP access to this instance after setup.</span></p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
  <p>&nbsp;</p>
</html>
