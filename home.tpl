<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>OpenVPN Connector</title>
  </head>
  <table cellspacing="15" cellpadding="15" style="width: 1000px;">
  <tbody>
  <tr>
  <td style="width: 983px;" colspan="2">
  <p><img style="display: block; margin-left: auto; margin-right: auto;" src="https://openvpn.net/images/openvpn.png" alt="OpenVPN Logo" width="292" height="56" /></p>
  <h1 style="text-align: center;">Connector for OpenVPN Cloud</h1>
  </td>
  </tr>
  <tr>
%if "Disconnected" in status:
  <td style="width: 675.233px;">
  <div>
  <h1 style="text-align: left;">Steps to connect to the VPN</h1>
  </div>
  <div style="text-align: left;">
  <p><strong>Step 1</strong>. <a href="https://openvpn.net/cloud-docs/owner-signup/">Signup</a> for an OpenVPN Cloud account at <a href="https://openvpn.net/">https://openvpn.net/</a></p>
  </div>
  <div style="text-align: left;">
  <p><strong>Step 2.</strong> <a href="https://openvpn.net/cloud-docs/adding-a-network/">Add a Network</a> with the subnet(s) of this virtual private cloud network</p>
  </div>
  <div style="text-align: left;">
  <p><strong>Step 3.</strong> From the <a href="https://openvpn.net/cloud-docs/installing-connector-for-linux/">Connector's download</a> drop-down list choose 'Connector on Linux and IaaS Cloud' and click on the 'Generate Token' button</p>
  </div>
  <div style="text-align: left;">
  <p><strong>Step 4.</strong> Copy and paste the token below</p>
  </div>

  <div style="text-align: center;"><form action="/submit" method="GET"><input maxlength="84" name="token" size="84" type="text" /><p> <input name="submit" type="submit" value="Submit" /></form></div>
  <div>
  <p style="text-align: left;">Instructions to setup your VPN for remote access and other scenarios are available <a href="https://openvpn.net/cloud-docs-category/vpn-setup-examples/">here</a></p>
  </div>
  </td>
%end
  <td style="width: 307.767px;background-color: #DFF7F7;">
  <div>
  <h1 style="text-align: left;">Connector Info</h1>
  </div>
  <div style="text-align: left;">
  <h2><strong>Version</strong></h2>
  </div>
  <div style="text-align: left;">
  %for line in version:  
  <div style="text-align: left;">{{line}}</div>
  %end
  </div>
  <div style="text-align: left;">
  <h2><strong>Status</strong></h2>
  </div>
  <div style="text-align: left;">
  <p>{{status}}</p>
  </div>
%if "Connected" in status:
  <div style="text-align: left;"><form action="/restart" method="GET"><input name="restart" type="submit" value="Reconnect" /></form></div>
<div style="text-align: left;">
  <h2><strong>Session Information</strong></h2>
  %for row in stats:
  </div>
  <div style="text-align: left;">&nbsp;{{row}}</div>
  %end
%end
  &nbsp;</td>
  </tr>
  <tr>
  <td style="width: 675.233px;" colspan="2">
  <div>
  <p><span style="color: #ff0000;">For improved security, please set network firewall to disable HTTP access to this instance after setup.</span></p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
  <div>&nbsp;</div>
</html>
