#!/bin/bash -e
while sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do sleep 1; done
wget https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub
sudo apt-key add openvpn-repo-pkg-key.pub && rm openvpn-repo-pkg-key.pub -f
sudo wget -O /etc/apt/sources.list.d/openvpn3.list https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-focal.list
sudo apt update
sudo apt install -y python3-openvpn-connector-setup
sudo sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo sed -i 's/#net.ipv6.conf.all.forwarding=1/net.ipv6.conf.all.forwarding=1/g' /etc/sysctl.conf
sudo sysctl -p
IF=`ip route | grep default | awk '{print $5}'`
sudo iptables -t nat -A POSTROUTING -o $IF -j MASQUERADE
sudo ip6tables -t nat -A POSTROUTING -o $IF -j MASQUERADE
sudo DEBIAN_FRONTEND=noninteractive apt install -y iptables-persistent
# download python3, bottle and dependencies
sudo apt -y install python3
sudo apt -y install python3-pip
sudo pip3 install bottle
sudo pip3 install python-pam
sudo pip3 install bash
#sudo git clone https://bitbucket.org/rkalbag/cloud-connector-web-frontend.git /root/cloud-connector-web-frontend
#configure webapp as service and start service
sudo mv /root/cloud-connector-web-frontend/connector_web.service /etc/systemd/system/
sudo chmod 644 /root/cloud-connector-web-frontend/o3.py
sudo systemctl enable connector_web.service
sudo systemctl daemon-reload
sudo service connector_web start
